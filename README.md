# About Me Webpage

This *about me* webpage is a simple example to teach some HTML and CSS to Prendiville students.

## Getting started

Clone this repository or copy the source, fill out *about-me.html* and place css in *styles.css*.
A completed example is on the branch *completed-example*.
